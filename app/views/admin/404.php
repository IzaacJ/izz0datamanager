	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Sorry! <small>We can't find that!</small></h1>
				<hr>
				<p>The controller generating this page is found at <code>APPPATH/classes/controller/admin.php</code>.</p>
				<p>This view is located at <code>APPPATH/views/admin/404.php</code>.</p>
			</div>
		</div>
	</div>