<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<?php echo Asset::js('https://code.jquery.com/jquery-2.1.3.min.js'); ?>
		<?php echo Asset::css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'); ?>
		<?php echo Asset::js('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js'); ?>
		<?php echo Asset::css('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'); ?>

		<?php echo Asset::js('https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.min.js'); ?>

		<?php echo Asset::js('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js'); ?>
		<?php echo Asset::css('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css'); ?>
		<?php echo Asset::js('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-buttons.js'); ?>
		<?php echo Asset::js('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-media.js'); ?>
		<?php echo Asset::js('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-thumbs.js'); ?>
		<?php echo Asset::css('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-buttons.css'); ?>
		<?php echo Asset::css('https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-thumbs.css'); ?>
		<!--<?php echo Asset::js('jquery-2.1.4.min.js'); ?>
		<?php echo Asset::css('bootstrap.css'); ?>
		<?php echo Asset::css('bootstrap-theme.css'); ?>
		<?php echo Asset::js('bootstrap.js'); ?>-->
		<?php echo Asset::css('izz0dm.css'); ?>
		<?php echo Asset::js('izz0dm.js'); ?>
		<?php if(Auth::check()) { ?>
			<?php echo Asset::js('izz0dma.js'); ?>
		<?php } ?>
        <?php $classes = get_declared_classes(); ?>
        <?php ob_start(); print_r($classes); $classes = ob_get_clean(); ?>

		<title>Izz0ware DataManager<?=(isset($title)?' - '.$title:'');?></title>
	</head>
	<body>
		<?= $header; ?>
		<div class="container">
			<div class="row">
			<?php if($sidebar) { ?>
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<?=$sidebar;?>
				</div>
				<div class="col-md-8">
			<?php } ?>
					<!--pre><?=$classes;?></pre-->
					<?php if($message) { ?>
						<div class="alert alert-info alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong><?=$message;?></strong>
						</div>
					<?php } ?>
					<?= $content; ?>
					<?php if($sidebar) { ?>
				</div>
			<?php } ?>
			</div>
		</div>
		<?= $footer; ?>
	</body>
</html>
