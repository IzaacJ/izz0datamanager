<footer class="navbar navbar-inverse navbar-fixed-bottom">
	<div class="container-fluid">
		<div class="navbar-header">
			<span class="navbar-text">&copy; <?= date("Y"); ?>, Izz0ware</span>
		</div>
		<div class="navbar-header pull-right">
			<span class="navbar-text">Powered by <a href="http://fuelphp.com" class="navbar-link">FuelPHP</a> <sup><?php echo Fuel::VERSION; ?></sup></span>
		</div>
	</div>
</footer>