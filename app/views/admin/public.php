<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 21:35
 */
?>
<form action="<?=APPURL;?>signin" method="post">
	<div class="panel panel-default middle middle-hauto middle-w350">
		<div class="panel-heading">
			<i class="glyphicon glyphicon-log-in"></i> Sign In
		</div>
		<div class="panel-body" style="margin: 10px;">
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
					<input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
					<input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="panel-footer" style="text-align: right;">
			<button type="submit" class="btn btn-success">Sign In</button>
		</div>
	</div>
</form>
