<header>
	<style>body { padding-top: 70px; }</style>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
                <?php if(Auth::check()) { ?>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#idm_menu-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <?php } ?>
				<a class="navbar-brand" href="<?=APPURL;?>"><i class="glyphicon glyphicon-floppy-saved"></i> Izz0ware DataManager</a>
            </div>
            <?php if(Auth::check()) { ?>
                <div class="collapse navbar-collapse" id="idm_menu-1">
                    <ul class="nav navbar-nav navbar-left" style="margin-left:15px; margin-top: 5px;">
                        <li><a href="<?=APPURL;?>"><i class="glyphicon glyphicon-eye-open"></i> Overview</a></li>
                        <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="glyphicon glyphicon-hdd"></i> Products <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="<?=APPURL;?>products"><i class="glyphicon glyphicon-list"></i> All Products</a></li>
								<li><a href="<?=APPURL;?>addproduct"><i class="glyphicon glyphicon-plus"></i> Add Product</a></li>
							</ul>
						</li>
                        <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="glyphicon glyphicon-cd"></i> Contents <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="<?=APPURL;?>items"><i class="glyphicon glyphicon-list"></i> All Contents</a></li>
                        		<li><a href="<?=APPURL;?>additem"><i class="glyphicon glyphicon-plus"></i> Add Content</a></li>
							</ul>
						</li>
                        <li style="height: 0px;margin-top: -20px; margin-bottom: -20px;"><hr></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" style="margin-top: 5px;">
                        <li><a href="<?=APPURL;?>profile"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                        <li><a href="<?=APPURL;?>signout"><i class="glyphicon glyphicon-log-out"></i> Sign out</a></li>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
</header>