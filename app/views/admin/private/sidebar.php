<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
?>
<div class="panel panel-default">
	<div class="panel-heading"><i class="glyphicon glyphicon-cog"></i> Menu</div>
	<div class="panel-body">
		<div class="container" style="padding-left: 5px;">
			<div class="row">
				<a href="<?=APPURL;?>"><i class="glyphicon glyphicon-eye-open"></i> Overview</a>
			</div>
			<div class="row" style="height: 10px;"></div>
			<div class="row">
				<a href="<?=APPURL;?>products"><i class="glyphicon glyphicon-list"></i> Products</a>
			</div>
			<div class="row">
				<a href="<?=APPURL;?>addproduct"><i class="glyphicon glyphicon-plus"></i> Add Product</a>
			</div>
			<div class="row" style="height: 10px;"></div>
			<div class="row">
				<a href="<?=APPURL;?>items"><i class="glyphicon glyphicon-list"></i> Contents</a>
			</div>
			<div class="row">
				<a href="<?=APPURL;?>additem"><i class="glyphicon glyphicon-plus"></i> Add Content</a>
			</div>
		</div>
	</div>
</div>