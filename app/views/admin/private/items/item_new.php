<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
$prods = Model_Product::get_items();
if(!$prods) Response::redirect('/addproduct');
$products = array();
foreach($prods as $p) {
	$products[$p['id']] = $p['productname'];
}
?>
<div class="container-fluid">
	<form action="<?=APPURL;?>additem/save" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">Title</span>
				<input type="text" name="contentname" class="form-control" placeholder="Title" value="<?=$contentname;?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">Version</span>
				<input type="text" name="version" class="form-control" placeholder="Version" value="<?=$version;?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">Description</span>
				<textarea name="description" class="form-control" rows="10"><?=$description;?></textarea>
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">File(s)</span>
				<input type="file" name="files[]" multiple class="form-control">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">GUID</span>
				<input type="text" name="guid" class="form-control" placeholder="GUID" value="<?=$guid;?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">Product</span>
				<select name="product" class="form-control" placeholder="Product">
					<?php
					foreach($products as $key => $value) {
						echo '<option value="'.$key.'" '.($product==$key?'selected':'').'>'.$value.'</option>';
					}
					?>
				</select>
			</div>
		</div>
		<div class="row">
			&nbsp;
		</div>
		<div class="row" style="text-align: right;">
			<a href="<?=APPURL;?>items" class="btn btn-danger">Cancel</a>
			<input type="submit" class="btn btn-success">
		</div>
	</form>
</div>