<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
if(!$item) { Response::redirect('/items'); }

$prods = Model_Product::get_items();
if(!$prods) Response::redirect('/addproduct');
$products = array();
foreach($prods as $p) {
	$products[$p['id']] = $p['productname'];
}
$item['uri'] = (is_array($item['uri'])?$item['uri']:unserialize($item['uri']));
?>
<div class="container-fluid">
	<form action="<?=APPURL;?>edititem/<?=$item['id'];?>/save" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">ID</span>
				<input type="text" name="id" class="form-control" placeholder="URI" aria-describedby="basic-addon1" readonly value="<?=$item['id']?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Title</span>
				<input type="text" name="contentname" class="form-control" placeholder="Title" aria-describedby="basic-addon1" value="<?=$item['contentname']?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Version</span>
				<input type="text" name="version" class="form-control" placeholder="Version" aria-describedby="basic-addon1" value="<?=$item['version']?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Description</span>
				<textarea name="description" class="form-control" rows="10"><?=$item['description'];?></textarea>
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">File(s)</span>
				<input type="file" name="files[]" multiple class="form-control" placeholder="URI" aria-describedby="basic-addon1" value="">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="current-files">Current Files</span>
				<!-- Item URL is in $item['uri'][1]  -->
				<ul class="list-group">
					<?php
					foreach($item['uri'] as $l) {
						?>
						<li class="list-group-item">
							<?=$l[1];?>
							<input type="hidden" name="currentfiles[]" value="<?=htmlspecialchars(json_encode($l), ENT_QUOTES);?>"/>
							<button type="button" class="btn btn-error right btn-deletefile" aria-label="Delete" data-value="<?=htmlspecialchars(json_encode($l), ENT_QUOTES);?>">
								<span class="glyphicon glyphicon-minus-sign"></span>
							</button>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">GUID</span>
				<input type="text" name="guid" class="form-control" placeholder="GUID" aria-describedby="basic-addon1" readonly value="<?=$item['guid']?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Product</span>
				<select name="product" class="form-control" placeholder="Product" readonly>
					<?php
					foreach($products as $key => $value) {
						echo '<option value="'.$key.'" '.($item['product']==$key?'selected':'').'>'.$value.'</option>';
					}
					?>
				</select>
			</div>
		</div>
		<div class="row" id="hidden_items">
			&nbsp;
		</div>
		<div class="row" style="text-align: right;">
			<a href="<?=APPURL;?>items" class="btn btn-danger">Cancel</a>
			<input type="submit" class="btn btn-success">
		</div>
	</form>
</div>