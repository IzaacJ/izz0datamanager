<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
if(!$item) { Response::redirect('/items'); }
?>
<div class="panel panel-warning">
	<div class="panel-heading">
		Really delete <?=$item['contentname'];?> ?
		<div class="pull-right" style="margin-top: -7px;">
			<?=$item['version'];?>
		</div>
	</div>
	<div class="panel-body">
		<div class="container-fluid">
			<?=$item['description'];?>
		</div>
	</div>
	<div class="panel-footer">
		<div class="pull-right" style="margin-top: -7px;">
			<a href="<?=APPURL;?>items" class="btn btn-default">No</a>
			<a href="<?=APPURL;?>deleteitem/<?=$item['id'];?>/true" class="btn btn-danger">YES, Delete it</a>
		</div>
        <?php $uris = (is_array($item['uri'])?$item['uri']:unserialize($item['uri']));
        $uri = '';
        foreach($uris as $l) {
            $uri .= $l[1] . '<br>';
        }
        ?>
        Files: <br><?=$uri;?>
	</div>
</div>