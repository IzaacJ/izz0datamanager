<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
if(!$item) { Response::redirect('/items'); }
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?=$item['contentname'];?>
		<div class="pull-right" style="margin-top: -7px;">
			<?=$item['version'];?>&nbsp;
			<a href="<?=APPURL;?>edititem/<?=$item['id'];?>" class="btn btn-info"><i class="fa fa-pencil"></i></a>
			<a href="<?=APPURL;?>deleteitem/<?=$item['id'];?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<div class="container-fluid">
			<?=$item['description'];?>
		</div>
	</div>
	<div class="panel-footer">
		<?php $uris = (is_array($item['uri'])?$item['uri']:unserialize($item['uri']));
		$uri = '';
		foreach($uris as $l) {
			$uri .= $l[1] . '<br>';
		}
		?>
		Files: <br><?=$uri;?>
	</div>
</div>