<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">Contents</div>
	<div class="panel-body">
		<div class="container-fluid">
			<div class="row hidden-sm hidden-xs">
				<!--div class="col-md-1">ID</div-->
				<div class="col-md-3">Name</div>
				<div class="col-md-1">Files</div>
				<div class="col-md-4">Description</div>
				<div class="col-md-2">Product</div>
				<div class="col-md-2">Version</div>
			</div>
			<hr>
			<?php
			if(count($items) < 1) { ?>
				<div class="row" style="text-align: center;">
					No contents found in the database!
				</div>
			<?php }
			foreach($items as $item) {
				$productname = Model_Product::get_item($item['product']);
				if($productname) {
					$productname = $productname['productname'];
				}else{
					$productname = 'Missing product ('.$item['product'].')';
				}
                $fs = (is_array($item['uri'])?$item['uri']:unserialize($item['uri']));
				$files = count($fs);
                $f = '';
                foreach($fs as $file) {
                    $f .= $file[1] . PHP_EOL;
                }
                $f = substr($f, 0, count($f)-4);
				?>
				<div class="row">
					<!--div class="col-md-1"><?=$item['id'];?></div-->
					<div class="col-md-3">
						<?=$item['contentname'];?><br>
						<a href="<?=APPURL;?>item/<?=$item['id'];?>">View</a> -
						<a href="<?=APPURL;?>edititem/<?=$item['id'];?>">Edit</a> -
						<a href="<?=APPURL;?>deleteitem/<?=$item['id'];?>">Delete</a>
					</div>
					<div class="col-md-1" data-toggle="tooltip" data-placement="top" title="<?=$f;?>"><?=$files;?></div>
					<div class="col-md-4"><?=$item['description'];?></div>
					<div class="col-md-2"><?=$productname;?></div>
					<div class="col-md-2"><?=$item['version'];?></div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</div>