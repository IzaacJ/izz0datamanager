<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">Overview</div>
	<div class="panel-body">
		<div class="container-fluid">
			<?php
			//DB::select()->from('products')->execute();
			$products = Model_Product::count_items();//DB::count_last_query();
			$itms = DB::select()->from('addons')->execute();
			$items = Model_Item::count_items();//DB::count_last_query();
			$files = 0;
			$size = 0;
			if($items > 0) {
				foreach($itms as $i) {
					$fls = unserialize($i['uri']);
					$files += count($fls);
				}
			}
			$size = FileUtilities::getFormatedFolderSize(APPROOT.'data'.DIRECTORY_SEPARATOR);
			DB::select()->from('users')->execute();
			$users =DB::count_last_query();
			?>
			<div class="row">
				<div class="col-md-4"><label>Products</label></div>
				<div class="col-md-8"><?=$products;?></div>
			</div>
			<div class="row">
				<div class="col-md-4"><label>Contents</label></div>
				<div class="col-md-8"><?=$items;?></div>
			</div>
			<div class="row">
				<div class="col-md-4"><label>Total number of files (size)</label></div>
				<div class="col-md-8"><?=$files;?> (<?=$size;?>)</div>
			</div>
			<div class="row">
				<div class="col-md-4"><label>Users</label></div>
				<div class="col-md-8"><?=$users;?></div>
			</div>
		</div>
	</div>
</div>