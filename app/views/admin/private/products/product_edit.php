<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
if(!$item) { Response::redirect('/products'); }
?>
<div class="container-fluid">
	<form action="<?=APPURL;?>editproduct/<?=$item['id'];?>/save" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">ID</span>
				<input type="text" name="id" class="form-control" placeholder="URI" aria-describedby="basic-addon1" readonly value="<?=$item['id']?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Title</span>
				<input type="text" name="productname" class="form-control" placeholder="Title" aria-describedby="basic-addon1" value="<?=$item['productname']?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Description</span>
				<textarea name="description" class="form-control" rows="10"><?=$item['description'];?></textarea>
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon">GUID</span>
				<input type="url" name="uri" class="form-control" placeholder="Store URI" value="<?=$item['uri'];?>">
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">GUID</span>
				<input type="text" name="guid" class="form-control" placeholder="GUID" aria-describedby="basic-addon1" readonly value="<?=$item['guid']?>">
			</div>
		</div>
		<div class="row">
			&nbsp;
		</div>
		<div class="row" style="text-align: right;">
			<a href="<?=APPURL;?>products" class="btn btn-danger">Cancel</a>
			<input type="submit" class="btn btn-success">
		</div>
	</form>
</div>