<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">Products</div>
	<div class="panel-body">
		<div class="container-fluid">
			<div class="row hidden-sm hidden-xs">
				<div class="col-md-1">ID</div>
				<div class="col-md-4">Name</div>
				<div class="col-md-1">Addons</div>
				<div class="col-md-6">Description</div>
			</div>
			<hr>
			<?php
			if(count($items) < 1) { ?>
				<div class="row" style="text-align: center;">
					No products found in the database!
				</div>
			<?php }
			foreach($items as $item) {
				$addons = $item['addons'];
				$cnt = count($addons);
				$f = '';
				foreach($addons as $addon) {
					$f .= $addon['contentname'].PHP_EOL;
				}
				?>
				<div class="row">
					<div class="col-md-1"><?=$item['id'];?></div>
					<div class="col-md-4">
						<?=$item['productname'];?><br>
						<a href="<?=APPURL;?>product/<?=$item['id'];?>">View</a> -
						<a href="<?=APPURL;?>editproduct/<?=$item['id'];?>">Edit</a> -
						<a href="<?=APPURL;?>deleteproduct/<?=$item['id'];?>">Delete</a>
					</div>
					<div class="col-md-1" data-toggle="tooltip" data-placement="top" title="<?=$f;?>"><?=$cnt;?></div>
					<div class="col-md-6"><?=$item['description'];?></div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</div>