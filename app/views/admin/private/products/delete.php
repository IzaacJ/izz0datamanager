<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
if(!$item) { Response::redirect('/products'); }
?>
<div class="panel panel-warning">
	<div class="panel-heading">
		Really delete <?=$item['productname'];?> ?
	</div>
	<div class="panel-body">
		<div class="container-fluid">
			<?=$item['description'];?>
		</div>
	</div>
	<div class="panel-footer">
		<div class="pull-right" style="margin-top: -7px;">
			<a href="<?=APPURL;?>products" class="btn btn-default">No</a>
			<a href="<?=APPURL;?>deleteproduct/<?=$item['id'];?>/true" class="btn btn-danger">YES, Delete it</a>
		</div>
		<?php
		$addons = DB::select()->from('addons')->where('product', $item['id'])->execute();
		$addonstext = '';
		if(DB::count_last_query() > 0) {
			foreach($addons as $addon) {
				$files = unserialize($addon['uri']);
				$addonstext .= $addon['contentname'] . ' ' . $addon['version'] . ' (' . count($files) . ' files)<br>';
			}
		}
		?>
		Addons: <br><?=($addonstext==''?'No addons found!':$addonstext);?>
	</div>
</div>