<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 23:49
 */
if(count($item) < 1) { Response::redirect('/products'); }
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<?=$item['productname'];?>
		<div class="pull-right" style="margin-top: -7px;">
			<?php if($item['uri']) { ?><a href="<?=$item['uri'];?>" class="btn btn-default"><i class="fa fa-link"></i></a><?php } ?>
			<a href="<?=APPURL;?>editproduct/<?=$item['id'];?>" class="btn btn-info"><i class="fa fa-pencil"></i></a>
			<a href="<?=APPURL;?>deleteproduct/<?=$item['id'];?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<div class="container-fluid">
			<?=$item['description'];?>
		</div>
	</div>
	<div class="panel-footer">
		<?php
		$addons = $item['addons'];
		$addonstext = '';
		if(DB::count_last_query() > 0) {
			foreach($addons as $addon) {
				$files = (is_array($addon['uri'])?$addon['uri']:unserialize($addon['uri']));
				$addonstext .= '<a href="/item/'.$addon['id'].'">'.$addon['contentname'] . '</a> ' . $addon['version'] . ' (' . count($files) . ' files)<br>';
			}
		}
		?>
		Addons: <br><?=($addonstext==''?'No addons found!':$addonstext);?>
	</div>
</div>