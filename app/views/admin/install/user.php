<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 22:58
 */

// TODO: Add more steps if needed.
?>
<div class="panel panel-default">
	<div class="panel-heading">Install Step 2 of # - Setup Administrator Account</div>
	<form action="<?=APPURL;?>install?user" method="post">
		<div class="panel-body">
			<div class="container-fluid">
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
						<input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
						<input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-at"></i></span>
						<input type="email" name="email" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer" style="text-align: right;"><input type="submit" class="btn btn-success" value="Next"></div>
	</form>
</div>
