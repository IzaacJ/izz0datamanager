<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-21
 * Time: 17:14
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">Install Step 1 of # - Database Setup</div>
	<form action="<?=APPURL;?>install?database" method="post">
		<div class="panel-body">
			<div class="container-fluid">
				<div class="row">
					Currently only MySQL is supported!
				</div>
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-globe"></i></span>
						<input type="text" name="mysqladdress" class="form-control" placeholder="Server Address" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
						<input type="text" name="mysqlusername" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
						<input type="text" name="mysqlpassword" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
						<input type="text" name="mysqldatabase" class="form-control" placeholder="Database Name" aria-describedby="basic-addon1">
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer" style="text-align: right;"><input type="submit" class="btn btn-success" value="Next"></div>
	</form>
</div>