<?php
return array(
	'_root_'  => 'admin/index',  // The default route
	'_404_'   => 'admin/404',    // The main 404 route

	'install' => 'admin/install', // The install route

	'products' => 'admin/product/products',
	'product(/:id)' => 'admin/product/viewproduct',
	'addproduct(/:action)?' => 'admin/product/addproduct',
	'editproduct(/:id)(/:action)?' => 'admin/product/editproduct',
	'deleteproduct(/:id)(/:really)?' => 'admin/product/deleteproduct',

	'items' => 'admin/item/items',
	'item(/:id)?' => 'admin/item/viewitem',
	'additem(/:action)?' => 'admin/item/additem',
	'edititem(/:id)(/:action)?' => 'admin/item/edititem',
	'deleteitem(/:id)(/:really)?' => 'admin/item/deleteitem',

	'signin' => 'admin/signin',
	'signout' => 'admin/signout',

	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
);
