<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2016-02-18
 * Time: 15:10
 */
class Model_Item extends Model
{
	private static $table = 'addons';

	public static function count_items() {
		DB::select()->from(self::$table)->execute();
		return DB::count_last_query();
	}

	public static function get_item($id) {
		$res = DB::select()->from(self::$table)->where('id', $id)->limit(1)->execute();
		if(DB::count_last_query() < 1) {
			return array();
		}
		$res = $res[0];
		$uris = (is_array($res['uri'])?$res['uri']:unserialize($res['uri']));
		$res['uri'] = $uris;
		return $res;
	}

	public static function get_items() {
		$res = DB::select()->from(self::$table)->execute();
		if(DB::count_last_query() < 1) {
			return array();
		}
		$items = array();
		foreach($res as $item) {
			$item['uri'] = (is_array($item['uri'])?$item['uri']:unserialize($item['uri']));
			$items[] = $item;
		}
		return $items;
	}

	public static function get_items_where($key, $op = null, $value = null) {
		if($op != null && $value == null) {
			$value = $op;
			$op = '=';
		}
		$ids = DB::select('id')->from(self::$table)->where($key, $op, $value)->execute();
		$result = array();

		foreach($ids as $id) {
			$result[] = self::get_item($id);
		}

		return $result;
	}

	public static function add_item($data) {
		list($id, $res) = DB::insert(self::$table)->set(array(
			'contentname' 	=> $data['contentname'],
			'version'		=> $data['version'],
			'description'	=> $data['description'],
			'guid'			=> $data['guid'],
			'product'		=> $data['product'],
		))->execute();
		$config = array(
			'path' => APPROOT.'data'.DIRECTORY_SEPARATOR.
				StringUtilities::slug_url($data['product']).DIRECTORY_SEPARATOR.
				StringUtilities::slug_url($id),
		);
		$uri = array();
		Upload::process($config);
		if(Upload::is_valid()) {
			Upload::save();
			$fls = Upload::get_files();
		}else{
			$fls = array();
		}
		foreach($fls as $item) {
			$uri[] = array($item['saved_as'], $item['name']);
		}
		$uris = serialize($uri);
		DB::update(self::$table)
			->set(array(
				'uri' => $uris,
			))->where('id', $id)
			->execute();

		return self::get_item($id);
	}

	public static function update_item($id, $data) {
		$item = self::get_item($id);
		if(!$item){
			return array();
		}
		$data = StringUtilities::parse_arrays(array(
			'contentname' => $item['contentname'],
			'product' => $item['product'],
			'version' => $item['version'],
			'description' => $item['description'],
			'uri' => array(),
			'currentfiles' => array(),
		), $data);
		$config = array(
			'path' => APPROOT.'data'.DIRECTORY_SEPARATOR.
				StringUtilities::slug_url($item['product']).DIRECTORY_SEPARATOR.
				StringUtilities::slug_url($id),
		);
		$currentfiles = array();
		$deletedfiles = array();

		if($data['currentfiles'] != array()) {
			foreach ( $data['currentfiles'] as $f ) {
				$currentfiles[] = json_decode( $f );
			}
		}
		$diff = StringUtilities::getArrDiff($item['uri'], $currentfiles);
		foreach($diff as $f) {
			$deleted = false;
			$deleted = Model_Item::delete_file($item['id'], $f);
			if(!$deleted) {
				$deletedfiles[] = $f;
			}
		}

		$uri = array();
		Upload::process($config);
		if(Upload::is_valid()) {
			Upload::save();
			$files = Upload::get_files();
		}else{
			$files = array();
		}
		foreach($files as $file) {
			$uri[] = array($file['saved_as'], $file['name']);
		}
		$uri = array_merge($uri, $currentfiles);
		$uris = serialize($uri);
		$updated = DB::update(self::$table)
			->set(array(
				'contentname' 	=> $data['contentname'],
				'version'		=> $data['version'],
				'description'	=> $data['description'],
				'product'		=> $data['product'],
				'uri'			=> $uris,
			))->where('id', $id)
			->execute();

		return array($updated, self::get_item($id), $deletedfiles);
	}

	public static function delete_item($id) {
		$item = self::get_item($id);
		$path = APPROOT.'data'.DIRECTORY_SEPARATOR.
			StringUtilities::slug_url($item['product']).DIRECTORY_SEPARATOR.
			StringUtilities::slug_url($id);
		try {
			File::delete_dir($path);
		}catch(Exception $ex) {
			// No action at the moment
		}
		$delete = DB::delete(self::$table)->where('id', $id)->execute();
		if($delete > 0) {
			return true;
		}
		return false;
	}

	public static function delete_file($id, $file) {
		$item = Model_Item::get_item($id);

		if(!$item) return array(false, 'Invalid item ID: '.$id);

		$path = APPROOT.'data'.DIRECTORY_SEPARATOR.
			StringUtilities::slug_url($item['product']).DIRECTORY_SEPARATOR.
			StringUtilities::slug_url($id).DIRECTORY_SEPARATOR.
			$file[0];
		try {
			$deleted = File::delete($path);
		}catch(Exception $ex) {
			$deleted = false;
		}
		/*
		if($deleted) {
			$newfiles = array();
			foreach($files as $fn) {
				if($file != $fn[0]) {
					$newfiles[] = $fn;
				}
			}
			$data = array(
				'uri' => $newfiles,
			);
			Model_Item::update_item($id, $data);
		}
		//*/
		return $deleted;
	}
}