<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2016-02-18
 * Time: 15:10
 */
class Model_Product extends Model_Crud
{
	private static $table = 'products';

	public static function count_items() {
		DB::select()->from(self::$table)->execute();
		return DB::count_last_query();
	}

	public static function get_item($id) {
		$res = DB::select()->from(self::$table)->where('id', $id)->limit(1)->execute();
		if(DB::count_last_query() < 1) {
			return array();
		}
		$res = $res[0];
		$res['addons'] = Model_Item::get_items_where('product', $id);

		return $res;
	}

	public static function get_items() {
		$res = DB::select()->from(self::$table)->execute();
		if(DB::count_last_query() < 1) {
			return array();
		}
		$items = array();
		foreach($res as $item) {
			$item['addons'] = Model_Item::get_items_where('product', $item['id']);
			$items[] = $item;
		}

		return $items;
	}

	public static function get_items_where($key, $op = null, $value = null) {
		if($op != null && $value == null) {
			$value = $op;
			$op = '=';
		}
		$ids = DB::select('id')->from(self::$table)->where($key, $op, $value)->execute();
		$result = array();

		foreach($ids as $id) {
			$result[] = self::get_item($id);
		}

		return $result;
	}

	public static function add_item($data) {
		if(self::get_items_where('guid', $data['guid'])) return array();
		list($id, $res) = DB::insert(self::$table)->set(array(
			'productname' 	=> $data['productname'],
			'description'	=> $data['description'],
			'guid'			=> $data['guid'],
			'uri'			=> $data['uri'],
		))->execute();

		return self::get_item($id);
	}

	public static function update_item($id, $data) {
		$item = self::get_item($id);
		if(!$item){
			return array();
		}
		$updated = DB::update(self::$table)->set(array(
				'productname' 	=> $data['productname'],
				'description'	=> $data['description'],
				'uri'			=> $data['uri'],
			))->where('id', $id)
			->execute();

		return array($updated, self::get_item($id));
	}

	public static function delete_item($id) {
		$item = self::get_item($id);
		$addons = Model_Item::get_items_where('product', $id);
		foreach($addons as $addon) {
			Model_Item::delete_item($addon['id']);
		}
		$path = APPROOT.'data'.DIRECTORY_SEPARATOR.
			StringUtilities::slug_url($item['id']);
		try {
			File::delete_dir($path);
		}catch(Exception $ex) {
			// No action at the moment
		}
		$delete = DB::delete(self::$table)->where('id', $id)->execute();
		if($delete > 0) {
			return true;
		}
		return false;
	}
}