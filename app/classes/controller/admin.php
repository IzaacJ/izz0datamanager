<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 21:08
 */

class Controller_Admin extends Controller
{
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$view = View::forge('admin/layout', array('title' => 'Overview'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->title = 'Authorized Access Only !';
			$view->content = View::forge('admin/public');
		}else{
			$view->content = View::forge('admin/private/general');
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_install()
	{
		$view = View::forge('admin/layout', array('title' => 'Installation'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(Config::get('db') === null) {
			$view->content = View::forge('admin/install/database');
		}else {
			DB::select()->from( 'users' )->execute();
			$ausrs = DB::count_last_query();

			if ( $ausrs === 0 ) {
				if (
					Input::post( 'username', null ) === null ||
					Input::post( 'password', null ) === null ||
					Input::post( 'email', null ) === null
				) {
					$view->content = View::forge( 'admin/install/user' );
				} else {
					$user = Auth::create_user( Input::post( 'username' ), Input::post( 'password' ), Input::post( 'email' ) );
					if ( ! $user ) {
						Response::redirect( '/install' );
					} else {
						Response::redirect( '/' );
					}
				}
			} else {
				Response::redirect( '/' );
			}
		}

		return $view;
	}

	public function action_signin() {
		if(
			null !== Input::post('username', null) &&
			null !== Input::post('password', null)
		) {
			$username = Input::post('username');
			$password = Input::post('password');

			Auth::login($username, $password);
		}

		Response::redirect('/');
	}
	public function action_signout() {
		Auth::logout();

		Response::redirect('/');
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		Response::redirect('/');
	}
}