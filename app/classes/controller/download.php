<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2015-12-20
 * Time: 21:08
 */

class Controller_Download extends Controller_Rest
{
	public function get_files() {
		// Received variables through a GET request:
		// id			The ID of the content package
		// signature	Signature to verify that the request is valid
		// Returns json or xml with list of urls to download and some general information.

		$id = Input::get('id');
		$signature = Input::get('signature');

		$res = array(
			'id' => '',
			'product' => '',
			'content' => '',
			'description' => '',
			'version' => '',
			'files' => array(),
			'message' => '',
			'code' => 200,
		);

		if( !$id ) {
			$res['message'] = 'No ID specified.';
			$res['code'] = 400;
		}else{
			// TODO: Implement some form of signature check to avoid malicious downloads
			/*if ( !$signature ) {
				// Not Implemented yet !
			}*/

			$res['id'] = $id;

			$content = Model_Item::get_item($id);

			if ( ! $content ) {
				$res['message'] = 'No content found.';
				$res['code'] = 404;
			}else{
				$res['product'] = $content['productname'];
				$res['content'] = $content['contentname'];
				$res['decription'] = $content['description'];
				$res['version'] = $content['version'];
				$files = unserialize($content['uri']);

				foreach($files as $file) {
					$res['files'][] = Uri::base(false).'/download/file.json?id='.$id.'&file='.$file[0];
				}
			}
		}

		return $this->response($res, $res['code']);
	}

	public function get_file() {
		$id = Input::get('id');
		$file = Input::get('file');

		$content = Model_Item::get_item($id);

		if ( ! $content ) {
			return $this->response(array('id' => $id, 'message' => 'No content found.', 'code' => 404), 404);
		}else{
			$files = (is_array($content['uri'])?$content['uri']:unserialize($content['uri']));
			$found = false;
			foreach($files as $f) {
				if($f[0] == $file) {
					$found = $f[1];
					break;
				}
			}
			if($found) {
				$path = APPROOT.'data'.DIRECTORY_SEPARATOR.
					StringUtilities::slug_url($content['product']).DIRECTORY_SEPARATOR.
					StringUtilities::slug_url($id).DIRECTORY_SEPARATOR.$file;
				File::download($path, $found);
			}else{
				return $this->response(array('id' => $id, 'message' => 'No valid file found.', 'code' => 404), 404);
			}
		}
	}
}