<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2016-02-16
 * Time: 22:09
 */

class Controller_Admin_Item extends Controller
{
	public function action_index()
	{
		$view = View::forge('admin/layout', array('title' => 'Overview'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			$view->content = View::forge('admin/private/general');
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_items()
	{
		$view = View::forge('admin/layout', array('title' => 'All Contents'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			$items = Model_Item::get_items();
			$view->content = View::forge('admin/private/items/items', array('items' => $items));
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_viewitem() {
		if($this->param('id'))
		{
			$id = $this->param('id');
		}else{
			Response::redirect('/items');
		}
		$item = Model_Item::get_item($id);
		$view = View::forge('admin/layout', array('title' => $item['contentname']));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			$item = View::forge('admin/private/items/item', array('item' => $item));
			$view->content = $item;
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_additem()
	{
		$prods = Model_Product::get_items();
		if(!$prods) Response::redirect('/addproduct');
		$products = array();
		foreach($prods as $p) {
			$products[] = $p['id'];
		}

		if($this->param('id'))
		{
			Response::redirect('/edititem/'.$this->param('id'));
		}
		$view = View::forge('admin/layout', array('title' => 'New Content'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		$uri = array();
		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			if(
				$this->param('action') === 'save' &&
				Input::post('contentname') != '' &&
				Input::post('version') != '' &&
				Input::post('guid') != '' &&
				Input::post('product') != ''
			) {
				$data = array(
					'contentname' => Input::post('contentname'),
					'version' => Input::post('version'),
					'description' => Input::post('description'),
					'guid' => Input::post('guid'),
					'product' => (in_array(Input::post('product'), $products)?Input::post('product'):0),
				);
				$item = Model_Item::add_item($data);
				$config = array(
					'path' => APPROOT.'data'.DIRECTORY_SEPARATOR.
						StringUtilities::slug_url($item['product']).DIRECTORY_SEPARATOR.
						StringUtilities::slug_url($item['id']),
				);
				Upload::process($config);
				if(Upload::is_valid()) {
					Upload::save();
					$fls = Upload::get_files();
				}else{
					$fls = array();
				}
				foreach($fls as $item) {
					$uri[] = array($item['saved_as'], $item['name']);
				}
				$uris = serialize($uri);
				$data = array(
					'uri' => $uris,
				);
				list($null, $item) = Model_Item::update_item($item['id'], $data);
				if($item['id'] > 0) {
					$view->message = "Added!";
					Response::redirect('item/'.$item['id']);
				}else{
					$view->message = "Failed to add new item!";
					$item = View::forge('admin/private/items/item_new');
					$item->contentname = Input::post('contentname');
					$item->version = Input::post('version');
					$item->description = Input::post('description');
					$item->uri = Input::post('files');
					$item->guid = Input::post('guid');
					$item->product = Input::post('product');
					$view->content = $item;
				}
			}else{
				$item = View::forge('admin/private/items/item_new');
				$item->contentname = (Input::post('contentname')!=''?Input::post('contentname'):'');
				$item->version = (Input::post('version')!=''?Input::post('version'):'');
				$item->description = (Input::post('description')!=''?Input::post('description'):'');
				$item->uri = '';
				$item->guid = (Input::post('guid')!=''?Input::post('guid'):'');
				$item->product = (Input::post('product')!=''?Input::post('product'):'');
				$view->content = $item;
			}
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_edititem()
	{
		if($this->param('id'))
		{
			$id = $this->param('id');
		}else{
			Response::redirect('/items');
		}
		$item = Model_Item::get_item($id);
		$view = View::forge('admin/layout', array('title' => 'Edit: '.$item['contentname']));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;
		$message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			if($this->param('action') === 'save') {
				$view->sidebar = View::forge('admin/private/sidebar');

				$data = array(
					'product' => Input::post('product'),
					'contentname' => Input::post('contentname'),
					'version' => Input::post('version'),
					'description' => Input::post('description'),
					'currentfiles' => Input::post('currentfiles'),
				);
				list($update, $updated, $msg) = Model_Item::update_item($id, $data);

				if($update > 0) {
					$message = "Updated!";
				}else{
					$message = "Update failed!";
				}
				if($msg) {
					if(is_array($msg)) {
						ob_start();
						print_r($msg);
						$msg = '<pre>'.ob_get_clean().'</pre>';
						$message .= $msg;
					}
				}
				$item = View::forge('admin/private/items/item_edit', array('item' => $updated));
				$view->message = $message;
				$view->content = $item;
			}else{
				$item = View::forge('admin/private/items/item_edit', array('item' => Model_Item::get_item($id)));
				$view->content = $item;
				$view->sidebar = View::forge('admin/private/sidebar');
			}
		}

		return $view;
	}

	public function action_deleteitem()
	{
		if(!Auth::check()) {
			Response::redirect('/items');
		}else{
			if($this->param('id')) {
				$id = $this->param('id');
				$item = Model_Item::get_item($id);
				$view = View::forge('admin/layout', array('title' => 'Delete: '.$item['contentname']));
				$view->header = View::forge('admin/header');
				$view->footer = View::forge('admin/footer');
				$view->sidebar = false;
				$view->message = false;

				if(!$this->param('really')) {
					$del = View::forge('admin/private/items/delete');
					$del->item = $item;
					$view->content = $del;
					return $view;
				}
			}else{
				Response::redirect('/items');
			}
			$item = Model_Item::get_item($id);
			$delete = false;
			if($item) {
				$delete = Model_Item::delete_item($id);
			}
			if($delete > 0) {
				$view->message = "Deleted!";
			}else{
				$view->message = "Deletion failed!";
			}
			Response::redirect('/items');
		}

		return $view;
	}
}