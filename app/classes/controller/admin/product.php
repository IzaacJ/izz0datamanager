<?php
/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2016-02-16
 * Time: 22:09
 */

class Controller_Admin_Product extends Controller
{
	public function action_index()
	{
		$view = View::forge('admin/layout', array('title' => 'Overview'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			$view->content = View::forge('admin/private/general');
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_products()
	{
		$view = View::forge('admin/layout', array('title' => 'All Products'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			$view->content = View::forge('admin/private/products/products', array('items' => Model_Product::get_items()));
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_viewproduct() {
		if($this->param('id'))
		{
			$id = $this->param('id');
		}else{
			Response::redirect('/products');
		}
		$item = Model_Product::get_item($id);
		$view = View::forge('admin/layout', array('title' => $item['productname']));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			$item = View::forge('admin/private/products/product', array('item' => Model_Product::get_item($id)));
			$view->content = $item;
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_addproduct()
	{
		if($this->param('id'))
		{
			Response::redirect('/editproduct/'.$this->param('id'));
		}
		$view = View::forge('admin/layout', array('title' => 'Add Product'));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;

		$uri = array();
		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			if($this->param('action') === 'save') {
				$data = array(
						'productname' => Input::post('productname'),
						'description' => Input::post('description'),
						'uri' => Input::post('uri'),
						'guid' => Input::post('guid')
				);
				$item = Model_Product::add_item($data);
				if($item) {
					$view->message = "Added!";
					Response::redirect('product/'.$item['id']);
				}else{
					$view->message = "Failed to add new product! Usually because the specified GUID already exists in the database.";
					$item = View::forge('admin/private/products/product_new');
					$item->description = Input::post('description');
					$item->uri = Input::post('uri');
					$item->guid = Input::post('guid');
					$item->productname = Input::post('productname');
					$view->content = $item;
				}
			}else{
				$item = View::forge('admin/private/products/product_new');
				$item->description = '';
				$item->uri = '';
				$item->guid = '';
				$item->productname = '';
				$view->content = $item;
			}
			$view->sidebar = View::forge('admin/private/sidebar');
		}

		return $view;
	}

	public function action_editproduct()
	{
		if($this->param('id'))
		{
			$id = $this->param('id');
		}else{
			Response::redirect('/products');
		}
		$item = Model_Product::get_item($id);
		$view = View::forge('admin/layout', array('title' => 'Edit: '.$item['productname']));
		$view->header = View::forge('admin/header');
		$view->footer = View::forge('admin/footer');
		$view->sidebar = false;
		$view->message = false;
		$message = false;

		if(!Auth::check()) {
			$view->content = View::forge('admin/public');
		}else{
			if($this->param('action') === 'save') {
				$view->sidebar = View::forge('admin/private/sidebar');

				$data = array(
					'productname' 	=> Input::post('productname'),
					'description' 	=> Input::post('description'),
					'uri'			=> Input::post('uri'),
				);
				list($update, $updated) = Model_Product::update_item($id, $data);
				if($update > 0) {
					$message = "Updated!";
				}else{
					$message = "Update failed!";
				}
				$item = View::forge('admin/private/products/product_edit', array('item' => $updated));
				$view->message = $message;
				$view->content = $item;
			}else{
				$item = View::forge('admin/private/products/product_edit', array('item' => Model_Product::get_item($id)));
				$view->content = $item;
				$view->sidebar = View::forge('admin/private/sidebar');
			}
		}

		return $view;
	}

	public function action_deleteproduct()
	{
		if(!Auth::check()) {
			Response::redirect('/products');
		}else{

			if($this->param('id')) {
				$id = $this->param('id');
				$item = Model_Product::get_item($id);
				$view = View::forge('admin/layout', array('title' => 'Delete: '.$item['productname']));
				$view->header = View::forge('admin/header');
				$view->footer = View::forge('admin/footer');
				$view->sidebar = false;
				$view->message = false;
				if(!$this->param('really')) {
					$del = View::forge('admin/private/products/delete');
					$del->item = Model_Product::get_item($id);
					$view->content = $del;
					return $view;
				}
			}else{
				Response::redirect('/products');
			}
			$product = Model_Product::get_item($id);
			$delete = false;
			if($product) {
				$delete = Model_Product::delete_item($id);
			}

			if($delete) {
				$view->message = "Deleted!";
			}else{
				$view->message = "Deletion failed!";
			}
			Response::redirect('/products');
		}

		return $view;
	}

}