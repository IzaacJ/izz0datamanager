<?php

/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2016-02-19
 * Time: 01:44
 */
class Controller_Data extends Controller_Rest {

	public function get_products() {
		$res = array(
			'products' => array(
				/*array(
					'id' => '',
					'productname' => '',
					'guid' => '',
					'uri' => '',
					'description' => '',
					'addons' => '',
				),*/
			),
			'message' => '',
			'code' => 200,
		);
		$items = Model_Product::get_items();
		if(!$items) {
			$res['message'] = 'No products found.';
			$res['code'] = 404;
		}else{
			$res['products'] = $items;
			$res['message'] = 'Products found: '.count($items);
		}

		return $this->response($res, $res['code']);
	}

	public function get_contents() {
		$res = array(
			'contents' => array(
				/*array(
					'id' => '',
					'contentname' => '',
					'guid' => '',
					'uri' => '',
					'description' => '',
					'product' => '',
					'version' => '',
				),*/
			),
			'message' => '',
			'code' => 200,
		);
		$items = Model_Item::get_items();
		if(!$items) {
			$res['message'] = 'No contents found.';
			$res['code'] = 404;
		}else{
			$res['contents'] = $items;
			$res['message'] = 'Contents found: '.count($items);
		}

		return $this->response($res, $res['code']);
	}

	public function get_contents_for_product() {
		$id = Input::get('id', 0);
		$res = array(
			'contents' => array(
				/*array(
					'id' => '',
					'contentname' => '',
					'guid' => '',
					'uri' => '',
					'description' => '',
					'product' => '',
					'version' => '',
				),*/
			),
			'message' => '',
			'code' => 200,
		);
		$items = Model_Item::get_items_where('product', $id);
		if(!$items) {
			$res['message'] = 'No contents found.';
			$res['code'] = 404;
		}else{
			$res['contents'] = $items;
			$res['message'] = 'Contents found: '.count($items);
		}

		return $this->response($res, $res['code']);
	}

	public function get_all_data() {
		$res = array(
			'products' => array(
				/*array(
					'id' => '',
					'productname' => '',
					'guid' => '',
					'uri' => '',
					'description' => '',
					'addons' => '',
				),*/
			),
			'contents' => array(
				/*array(
					'id' => '',
					'contentname' => '',
					'guid' => '',
					'uri' => '',
					'description' => '',
					'product' => '',
					'version' => '',
				),*/
			),
			'message' => '',
			'code' => 200,
		);
		$products = Model_Product::get_items();
		$contents = Model_Item::get_items();
		if(!$products) {
			$res['message'] = 'No products found.';
		}else{
			$res['products'] = $products;
			$res['message'] = 'Products found: '.count($products);
		}
		if(!$contents) {
			$res['message'] .= ' No contents found.';
		}else{
			$res['contents'] = $contents;
			$res['message'] .= ' Contents found: '.count($contents);
		}

		if(!$products && !$contents) $res['code'] = 404;

		return $this->response($res, $res['code']);
	}
}