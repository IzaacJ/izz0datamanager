<?php

/**
 * Created by PhpStorm.
 * User: izz0j
 * Date: 2016-02-18
 * Time: 02:42
 */
class FileUtilities {

	public static function getFormatedFolderSize($path) {
		return self::format_size(self::foldersize($path));
	}

	public static function foldersize($path) {
		$total_size = 0;
		$files = scandir($path);
		$cleanPath = rtrim($path, '/'). '/';

		foreach($files as $t) {
			if ($t<>"." && $t<>"..") {
				$currentFile = $cleanPath . $t;
				if (is_dir($currentFile)) {
					$size = self::foldersize($currentFile);
					$total_size += $size;
				}
				else {
					$size = filesize($currentFile);
					$total_size += $size;
				}
			}
		}

		return $total_size;
	}

	public static function format_size($size) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');

		$mod = 1024;

		for ($i = 0; $size > $mod; $i++) {
			$size /= $mod;
		}

		$endIndex = strpos($size, ".")+3;

		return substr( $size, 0, $endIndex).' '.$units[$i];
	}

}