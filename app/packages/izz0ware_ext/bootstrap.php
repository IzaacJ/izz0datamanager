<?php
// Add namespace, necessary if you want the autoloader to be able to find classes
Autoloader::add_namespace('Izz0ware', __DIR__.'/classes/');

// Add as core namespace
Autoloader::add_core_namespace('Izz0ware');

// Add as core namespace (classes are aliased to global, thus useable without namespace prefix)
// Set the second argument to true to prefix and be able to overwrite core classes
Autoloader::add_core_namespace('Izz0ware', true);

// And add the classes, this is useful for:
// - optimization: no path searching is necessary
// - it's required to be able to use as a core namespace
// - if you want to break the autoloader's path search rules
Autoloader::add_classes(array(
	'Izz0ware\\StringUtilities' => __DIR__.'/classes/string_util.php',
	'Izz0ware\\FileUtilities' => __DIR__.'/classes/file_util.php',
	//'Izz0ware\\Anotherclass' => __DIR__.'/classes/anotherclass.php',
));