jQuery(document).ready(function($) {
	$('.btn-deletefile').click(function() {
		var file = this.dataset.value;
		var filename = window.JSON.parse(file)[1];
		if(confirm('Do you really want to mark this file for deletion?'+filename)) {
			$(this).parent('li.list-group-item').css('text-decoration', 'line-through');
			$(this).css('display', 'none');
			$(this).siblings('input[type=hidden]').remove();
		}
	});

	function htmlEscape(str) {
		return String(str)
			.replace(/&/g, '&amp;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#39;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;');
	}

	function htmlUnescape(value){
		return String(value)
			.replace(/&quot;/g, '"')
			.replace(/&#39;/g, "'")
			.replace(/&lt;/g, '<')
			.replace(/&gt;/g, '>')
			.replace(/&amp;/g, '&');
	}
});